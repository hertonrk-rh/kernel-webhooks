"""Manages the cki:: label."""
from dataclasses import dataclass
import enum
import sys

from cki_lib import logger
from cki_lib.gitlab import get_instance
from gql import gql

from . import common
from . import defs

LOGGER = logger.get_logger('cki.webhook.ckihook')


class Status(enum.IntEnum):
    # pylint: disable=invalid-name
    """Possible status of a pipeline."""

    Unknown = 0
    Missing = 1
    Failed = 2
    Canceled = 3
    Running = 4
    Pending = 4
    OK = 5
    Success = 5


@dataclass
class MrPipeline:
    # pylint: disable=too-many-instance-attributes
    """A simple object to parse and describe a graphql pipeline result."""

    pipeline: dict
    mr_labels: list

    def __post_init__(self):
        """Initialize the pipeline's data."""
        self.label_changed = False
        self.existing_label = None
        self.new_label = None
        if not self.pipeline:
            self.pipeline = {}
        self.name = self.pipeline.get('sourceJob', {}).get('name')
        self._set_status(self.pipeline.get('status', 'Unknown').title())
        self._set_failed_stage(self.pipeline.get('jobs', {}).get('nodes', []))
        self._set_label_prefix()

        if self.name in defs.KERNEL_PIPELINES + defs.KERNEL_RT_PIPELINES:
            self.existing_label = find_label(self.mr_labels, self.label_prefix)
            self.new_label = self._label_string()
        if self.existing_label != self.new_label:
            self.label_changed = True

    def _set_failed_stage(self, failed_jobs):
        self.failed_stage = failed_jobs[0]['stage']['name'] if self.status is \
                            Status.Failed else None

    def _set_label_prefix(self):
        if self.name in defs.KERNEL_PIPELINES:
            self.label_prefix = defs.CKI_KERNEL_PREFIX
            return
        if self.name in defs.KERNEL_RT_PIPELINES:
            self.label_prefix = defs.CKI_KERNEL_RT_PREFIX
            return
        self.label_prefix = None

    def _set_status(self, status):
        self.status = next((member for name, member in Status.__members__.items()
                            if status == name), Status.Unknown)

    def _label_string(self):
        return f'{self.label_prefix}::{self.status.name}::{self.failed_stage}' \
            if self.failed_stage else f'{self.label_prefix}::{self.status.name}'


EMPTY_PIPELINES = [{'sourceJob': {'name': 'merge_request_regular'},
                    'status': 'Missing'},
                   {'sourceJob': {'name': 'realtime_check_regular'},
                    'status': 'Missing'}
                   ]


# All the data we need to process the MR in one shot.
CKIHOOK_QUERY = gql("""
query mrData($mr_id: String!, $namespace: ID!)
{
  currentUser {
    username
  }
  project(fullPath: $namespace) {
    mergeRequest(iid: $mr_id) {
      labels {
        nodes {
          title
          color
          description
        }
      }
      headPipeline {
        downstream {
          nodes {
            status
            sourceJob {
              name
            }
            jobs(statuses: [FAILED]) {
              nodes {
                status
                stage {
                  name
                }
              }
            }
          }
        }
      }
    }
  }
}
""")


def update_cki_labels(namespace, mr_id, pipelines):
    """Update the CKI labels of the given MR."""
    gl_instance = get_instance('https://gitlab.com')
    gl_project = gl_instance.projects.get(namespace)
    labels = [pipe.new_label for pipe in pipelines]
    common.add_label_to_merge_request(gl_instance, gl_project, mr_id, labels, remove_scoped=True)


def find_label(mr_labels, key):
    """Return the scoped label matching key string."""
    return next((label['title'] for label in mr_labels if
                 label['title'].startswith(f'{key}::')), None)


def do_ckihook_query(username, query_params):
    """Do the query."""
    with common.gl_graphql_client() as client:
        gql_data = client.execute(CKIHOOK_QUERY, variable_values=query_params)

    if not {'currentUser', 'project'} <= gql_data.keys():
        LOGGER.error('Gitlab did not return the expected data for %s.', query_params)
        return None
    # Ignore bot messages.
    if gql_data['currentUser']['username'] == username:
        LOGGER.info('Ignoring bot message.')
        return None
    # Now we just need the MR details.
    return gql_data['project'].get('mergeRequest')


def compute_labels(username, namespace, mr_id):
    """Compute and apply a CKI label for the given MR."""
    LOGGER.info('Computing CKI labels for %s!%s.', namespace, mr_id)

    # Get the MR details from the graphql API.
    query_params = {'namespace': namespace, 'mr_id': str(mr_id)}
    if not (mr_data := do_ckihook_query(username, query_params)):
        return

    # If there is no pipeline data then fake one to produce the Missing label we want.
    if not mr_data.get('headPipeline'):
        LOGGER.info('No Head Pipeline, faking it 😬.')
        mr_data['headPipeline'] = {'downstream': {'nodes': EMPTY_PIPELINES}}

    # For each pipeline, get the existing label and calculate a new label.
    updated_pipelines = []
    pipelines = mr_data['headPipeline']['downstream']['nodes']
    for pipeline in pipelines:
        pipeline = MrPipeline(pipeline, mr_data['labels'].get('nodes', []))
        if not pipeline.label_changed:
            LOGGER.info('%s label has not changed: %s', pipeline.name, pipeline.new_label)
            continue
        LOGGER.info('%s label has changed from %s to %s', pipeline.name, pipeline.existing_label,
                    pipeline.new_label)
        updated_pipelines.append(pipeline)

    # Do something?
    if updated_pipelines:
        update_cki_labels(namespace, mr_id, updated_pipelines)


def process_message(msg, **_):
    """Process a GL event."""
    if msg.payload['object_kind'] == 'pipeline':
        if msg.payload['merge_request'] is None:
            LOGGER.info('Pipeline %s is not associated with an MR, ignoring.',
                        msg.payload['object_attributes']['id'])
            return
        mr_id = msg.payload['merge_request']['iid']
    elif msg.payload['object_kind'] == 'merge_request':
        mr_id = msg.payload["object_attributes"]["iid"]
        # If the MR already has CKI labels then ignore this MR event as any existing
        # label will be kept up to date by pipeline events.
        labels = msg.payload['labels']
        if labels and find_label(labels, defs.CKI_KERNEL_PREFIX) \
                and find_label(labels, defs.CKI_KERNEL_RT_PREFIX) \
                and not common.has_label_changed(msg.payload, f'{defs.CKI_KERNEL_PREFIX}::',
                                                 common.LabelPart.PREFIX) \
                and not common.has_label_changed(msg.payload, f'{defs.CKI_KERNEL_RT_PREFIX}::',
                                                 common.LabelPart.PREFIX):
            LOGGER.info('MR %d already has CKI labels, ignoring event.', mr_id)
            return
    else:
        LOGGER.warning('Ignoring event with unexpected object_kind %s.', msg.payload['object_kind'])
        return

    namespace = msg.payload['project']['path_with_namespace']
    compute_labels(msg.payload['user']['username'], namespace, mr_id)


WEBHOOKS = {
    'merge_request': process_message,
    'pipeline': process_message,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('CKIHOOK')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS, get_gl_instance=False)


if __name__ == "__main__":
    main(sys.argv[1:])
