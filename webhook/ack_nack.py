"""Process all of the ACKs/NACKs that are associated with a merge request."""
import datetime
import re
import sys

from cki_lib import logger
from cki_lib import misc

from . import cdlib
from . import common
from . import defs

LOGGER = logger.get_logger('cki.webhook.ack_nack')

APPROVAL_RULE_ACKS = ('- Approval Rule "%s" requires at least %d ACK(s) (%d given) '
                      'from set (%s).  \n')
APPROVAL_RULE_OKAY = ('- Approval Rule "%s" already has %d ACK(s) (%d required).  \n')
APPROVAL_RULE_OPT = ('- Approval Rule "%s" requests optional ACK(s) from set (%s).  \n')
STALE_APPROVALS_SUMMARY = ('Note: Approvals from %s have been invalidated due to code changes in '
                           'this merge request. Please re-approve, if appropriate.')


def _save(gl_instance, gl_project, gl_mergerequest, new_approval, create_gl_status_note,
          status, subsys_scoped_labels, message):
    # pylint: disable=too-many-arguments
    note = f'ACK/NACK Summary: {status} - {message}'
    LOGGER.info(note)

    labels = [f'Acks::{status}'] + subsys_scoped_labels
    common.add_label_to_merge_request(gl_instance, gl_project, gl_mergerequest.iid, labels)

    if misc.is_production() and not common.mr_is_closed(gl_mergerequest):
        if new_approval is not None:
            gl_mergerequest.notes.create({'body': new_approval[1]})
        if create_gl_status_note:
            gl_mergerequest.notes.create({'body': note})


def _emails_to_gl_user_ids(gl_instance, reviewers):
    # Search GitLab for users with a public email address set.
    users = []
    for reviewer in reviewers:
        userlist = gl_instance.users.list(search=reviewer)
        for user in userlist:
            users.append(user.id)
    return users


def _ensure_base_approval_rule(gl_project, gl_mergerequest):
    """Ensure base approval rule exists and has right number of reviewers set."""
    if gl_mergerequest.state != "opened" or gl_mergerequest.draft:
        return

    proj_ar_list = gl_project.approvalrules.list(search='All Members')
    if len(proj_ar_list) < 1:
        LOGGER.warning("Project %s has no 'All Members' approval rule", gl_project.name)
        return

    proj_all_members = proj_ar_list[0]
    proj_rule_id = proj_all_members.id
    proj_rule_name = proj_all_members.name
    proj_reqs = proj_all_members.approvals_required
    if proj_reqs == 0:
        LOGGER.warning("Project %s isn't requiring any reviewers for MRs", gl_project.name)
        return

    ar_list = gl_mergerequest.approval_rules.list()
    for approval_rule in ar_list:
        if approval_rule.name == 'All Members':
            if approval_rule.approvals_required == 0:
                LOGGER.warning("%s MR %d had approvals required set to 0",
                               gl_project.name, gl_mergerequest.iid)
                approval_rule.approvals_required = proj_reqs
                approval_rule.save()
            else:
                LOGGER.debug("%s MR %d has 'All Members' rule set appropriately",
                             gl_project.name, gl_mergerequest.iid)
            return

    LOGGER.warning("%s MR %d had no base approvals required",
                   gl_project.name, gl_mergerequest.iid)
    gl_mergerequest.approval_rules.create({'approval_project_rule_id': proj_rule_id,
                                           'name': proj_rule_name,
                                           'approvals_required': proj_reqs})


def _edit_approval_rule(gl_instance, gl_mergerequest, subsystem, reviewers, num_req):
    """Add a GitLab Approval Rule named subsystem, with num_req required reviewers."""
    rule_exists = False
    rule = None
    current_rules = gl_mergerequest.approval_rules.list(as_list=False)
    for rule in current_rules:
        if rule.name == subsystem:
            rule_exists = True
            break

    if rule_exists:
        LOGGER.info("Approval rule for subsystem %s already exists", subsystem)
        return

    LOGGER.info("Create rule for ss %s, with %d required approval(s) from user(s) %s",
                subsystem, num_req, reviewers)

    user_ids = _emails_to_gl_user_ids(gl_instance, reviewers)
    LOGGER.debug("Approval Rule user ids: %s", user_ids)

    if not misc.is_production():
        return

    gl_mergerequest.approvals.set_approvers(num_req, approver_ids=user_ids, approver_group_ids=[],
                                            approval_rule_name=subsystem)


def _get_reviewers(gl_instance, gl_mergerequest, changed_files, owners_parser, submitter_email):
    """Parse the owners.yaml file and return a set of email addresses and subsystem labels."""
    # pylint: disable=too-many-branches,too-many-locals
    if not changed_files:
        return [], []

    opt_reviewers = {}
    req_reviewers = {}
    all_reviewers = {}
    # A merge request can span multiple subsystems so get the reviewers for each subsystem.
    # Call owners parser individually for each file.
    for changed_file in changed_files:
        for entry in owners_parser.get_matching_entries([changed_file]):
            ss_label = entry.get_subsystem_label()
            required_approvals = entry.get_required_approvals()
            # Get all maintainers and reviewers
            maintainers = entry.get_maintainers()
            reviewers = entry.get_reviewers()
            if maintainers is None or reviewers is None:
                LOGGER.warning("Got maintainers: %s, reviewers: %s, for entry %s",
                               maintainers, reviewers, entry)
                continue
            for user in [*maintainers, *reviewers]:
                # Ensure that the merge request submitter doesn't show up in the reviewers list
                if not user or user['email'] == submitter_email:
                    continue

                if ss_label not in opt_reviewers:
                    opt_reviewers[ss_label] = set([])
                    if required_approvals:
                        req_reviewers[ss_label] = set([])

                if ss_label not in all_reviewers:
                    all_reviewers[ss_label] = set([])

                all_reviewers[ss_label].update([user['email']])

                # Ensure that restricted users aren't listed as required reviewers
                if required_approvals and not user.get('restricted', False):
                    req_reviewers[ss_label].update([user['email']])
                else:
                    opt_reviewers[ss_label].update([user['email']])

    LOGGER.debug('Subsystem optional reviewers: %s', opt_reviewers)
    LOGGER.debug('Subsystem required reviewers: %s', req_reviewers)

    ss_reviewers = []
    for subsystem, reviewers in all_reviewers.items():
        ss_reviewers.append((reviewers, subsystem))

    for subsystem, reviewers in opt_reviewers.items():
        if reviewers:
            _edit_approval_rule(gl_instance, gl_mergerequest, subsystem, reviewers, 0)

    required = []
    for subsystem, reviewers in req_reviewers.items():
        required.append((reviewers, subsystem))
        if reviewers:
            _edit_approval_rule(gl_instance, gl_mergerequest, subsystem, reviewers, 1)

    return ss_reviewers, required


def get_ark_config_mr_ccs(merge_request):
    """Return a list of any CC email addresses from an ark project MR description."""
    cc_list = []
    if not merge_request.description:
        return cc_list
    mlines = merge_request.description.splitlines()
    for line in mlines:
        if line.startswith('Cc: ') and line.endswith('@redhat.com>'):
            cc_list.append(line.split()[-1].split('<')[1].split('>')[0])
    return cc_list


def get_ark_reviewers(project_id, merge_request, files):
    """Return the min number of reviews for the project and for ARK try to include reviewer set."""
    # For ark kernel config changes, also return users in th MR's CC line.
    if project_id == defs.ARK_PROJECT_ID and all(file.startswith('redhat/configs/')
                                                 for file in files):
        cc_reviewers = get_ark_config_mr_ccs(merge_request)
        if cc_reviewers:
            return [(set(cc_reviewers), None)]

    return None


def _validate_note_submitter(message, note_email, username):
    """Parse the message to make sure people aren't forging acks/nacks via email bridge."""
    if username != defs.EMAIL_BRIDGE_ACCOUNT:
        return True

    bridged_email = None
    for line in message.split('\n'):
        pattern = " commented via email:"
        if not line.endswith(pattern):
            continue

        bridged_email = line.split(pattern)[0].split(' ')[-1].replace('<', '').replace('>', '')
        break

    if bridged_email != note_email:
        LOGGER.warning("Rejecting apparent forged ack/nack attempt (%s != %s)",
                       bridged_email, note_email)
        return False

    return True


def _parse_tag(message):
    """Parse an individual message and look for any tags of interest."""
    # This allows the signoff email brackets (<>) to be escaped with a single backslash.
    regex = (r'(?P<tag>((Na|A)cked-by|Re(scind|voke)-(N|n)?(A|a)cked-by)): '
             r'(?P<name>[^<\\]*) \\?<(?P<email>[^>\\]*)\\?>')
    tag_re = re.compile(regex)
    for line in message.split('\n'):
        match = tag_re.match(line)
        if not match:
            continue

        tag = match.group('tag')
        name = match.group('name')
        email = match.group('email')
        return (tag, name, email)

    return (None, None, None)


def _tag_email_error_message(tag, submitter_email, note_email, public_email, username):
    # cki-bot / cki-kwf-bot can impersonate other users for the approve/unapprove button.
    # redhat-patchlab leaves messages on behalf of users via the email bridge.
    if username in defs.BOT_ACCOUNTS:
        return None

    if tag in ('Acked-by', 'Rescind-acked-by', 'Revoke-acked-by') and submitter_email == note_email:
        return f'{submitter_email} cannot self-ack merge request'

    if not public_email:
        return (f"Ignoring '{tag} <{note_email}>' since user does not have a public "
                "email address on their GitLab profile. Click on your avatar at the top "
                "right, click [Edit profile](https://gitlab.com/-/profile), and ensure "
                "that your redhat.com address is set to your public email.")

    if public_email != note_email:
        return (f"Ignoring '{tag} <{note_email}>' since this doesn't match the "
                f"user's public email address {public_email} on GitLab. You can change "
                "this by clicking on your avatar at the top right, click "
                "[Edit profile](https://gitlab.com/-/profile), and ensure that your redhat.com "
                "address is set to your public email.")

    return None


def _process_acks_nacks(approval_data, messages, last_diff_ts, submitter_email,
                        gl_instance, gl_project, gl_mergerequest, rhkernel_src):
    # pylint: disable=too-many-branches,too-many-locals,too-many-arguments,too-many-statements
    """Process a list of messages and collect any ACKs/NACKS."""
    acks = set([])
    nacks = set([])
    valid_ack_nack = set([])
    invalidated_acks = set([])
    code_changes_checked = False
    message_before_ccts = False
    report = False
    code_changed_ts = cdlib.get_last_code_changed_timestamp(gl_mergerequest)
    # append in-flight approval/unapproval from button press
    if approval_data is not None:
        messages.append(approval_data)
    for message_timestamp, message, public_email, username in messages:
        (tag, name, note_email) = _parse_tag(message)
        if not tag:
            continue

        if not _validate_note_submitter(message, note_email, username):
            continue

        if not code_changes_checked:
            (code_changed, report) = cdlib.mr_code_changed(gl_instance, gl_project,
                                                           gl_mergerequest, rhkernel_src)
            code_changes_checked = True
        if code_changed_ts is not None:
            message_before_ccts = message_timestamp < code_changed_ts
        if last_diff_ts and (code_changed or message_before_ccts):
            ack_valid = last_diff_ts < message_timestamp
            if not ack_valid:
                LOGGER.warning("Ignoring '%s: %s <%s>' since code was changed at %s after ACKs",
                               tag, name, note_email, last_diff_ts)
                if report:
                    invalidated_acks.add(note_email)
        else:
            ack_valid = True

        nack_valid = True
        tag_error = _tag_email_error_message(tag, submitter_email, note_email, public_email,
                                             username)
        if tag_error:
            LOGGER.warning(tag_error)
            nack_valid = False
            ack_valid = False

        LOGGER.debug("Processing '%s: %s <%s>' ts=%s email=%s ack_valid=%s, nack_valid=%s",
                     tag, name, note_email, message_timestamp, public_email, ack_valid, nack_valid)

        name_email = (name, note_email)
        if tag == 'Acked-by' and ack_valid:
            acks.add(name_email)
            valid_ack_nack.add(note_email)
            if name_email in nacks:
                nacks.remove(name_email)
        elif tag in ('Rescind-acked-by', 'Revoke-acked-by',
                     'Rescind-Acked-by', 'Revoke-Acked-by') and ack_valid:
            if name_email in acks:
                acks.remove(name_email)
                valid_ack_nack.remove(note_email)
            else:
                LOGGER.warning('Cannot find ACK to revoke for %s', name_email)
        elif tag == 'Nacked-by' and nack_valid:
            nacks.add(name_email)
            valid_ack_nack.add(note_email)
        elif tag in ('Rescind-nacked-by', 'Revoke-nacked-by',
                     'Rescind-Nacked-by', 'Revoke-Nacked-by') and nack_valid:
            if name_email in nacks:
                nacks.remove(name_email)
                valid_ack_nack.remove(note_email)
            else:
                LOGGER.warning('Cannot find NACK to revoke for %s', name_email)
        elif ack_valid:
            LOGGER.warning("Unhandled tag: %s (from %s <%s>)", tag, name_email[0], name_email[1])

    for email in valid_ack_nack:
        if email in invalidated_acks:
            invalidated_acks.remove(email)
    return (acks, nacks, invalidated_acks, report)


def _show_ack_nacks(ack_nacks):
    summary = [f'{ack_nack[0]} <{ack_nack[1]}>' for ack_nack in ack_nacks]
    summary.sort()
    count = len(summary)
    return (", ".join(summary), count)


def _has_acknack(ack_nacks, subsystem_reviewers):
    for ack_nack in ack_nacks:
        if ack_nack[1] in subsystem_reviewers:
            return True

    return False


def _get_old_subsystems_from_labels(gl_mergerequest):
    subsys_list = []
    subsys_labels = []
    for label in gl_mergerequest.labels:
        if label.startswith('Acks::'):
            sslabel_parts = label.split("::")
            if len(sslabel_parts) == 3:
                subsys_list.append(sslabel_parts[1])
                subsys_labels.append(label)
    return (subsys_list, subsys_labels)


def _get_stale_labels(old_subsystems, old_labels, subsys_scoped_labels):
    stale_labels = []
    for subsystem in old_subsystems:
        if subsystem not in subsys_scoped_labels.keys():
            for label in old_labels:
                if label.startswith(f"Acks::{subsystem}::"):
                    stale_labels.append(label)
    LOGGER.debug("Stale labels: %s", stale_labels)
    return stale_labels


def _get_subsys_scoped_labels(gl_project, gl_mergerequest, acks, nacks, req_reviewers):
    # pylint: disable=too-many-locals,too-many-arguments
    subsys_scoped_labels = {}
    subsys_without_acks = set([])
    (old_subsystems, old_labels) = _get_old_subsystems_from_labels(gl_mergerequest)

    if req_reviewers is not None:
        for subsystem_reviewers, subsystem_label in req_reviewers:
            subsystem_has_an_ack = _has_acknack(acks, subsystem_reviewers)
            subsystem_has_a_nack = _has_acknack(nacks, subsystem_reviewers)

            if subsystem_has_a_nack:
                scoped_label = 'NACKed'
            elif not subsystem_has_an_ack:
                subsys_without_acks.add(frozenset(subsystem_reviewers))
                scoped_label = defs.NEEDS_REVIEW_SUFFIX
            else:
                scoped_label = defs.READY_SUFFIX

            if subsystem_label:
                subsys_scoped_labels[subsystem_label] = scoped_label

    stale_labels = _get_stale_labels(old_subsystems, old_labels, subsys_scoped_labels)
    if stale_labels:
        common.remove_labels_from_merge_request(gl_project, gl_mergerequest.iid, stale_labels)

    ret = [f'Acks::{x}::{y}' for x, y in subsys_scoped_labels.items()]
    ret.sort()  # Sort the subsystem scoped labels for the tests.

    return (ret, subsys_without_acks)


def _get_ar_acks_nacks(gl_instance, ar_reviewers, summary, user_cache):
    # pylint: disable=too-many-locals
    needed = []
    optional = []
    provided = []

    req_approvals = 0
    for subsystem, required, given, _, user_ids in ar_reviewers:
        emails = []
        for user_id in user_ids:
            if (public_email := _lookup_gitlab_email(gl_instance, user_id, user_cache)) != "":
                emails.append(public_email)
        reviewers = list(emails)
        reviewers.sort()
        reviewers = ', '.join(reviewers)
        if required > 0:
            if given >= required:
                provided.append(APPROVAL_RULE_OKAY % (subsystem, given, required))
            else:
                req_approvals += required - given
                needed.append(APPROVAL_RULE_ACKS % (subsystem, required, given, reviewers))
        elif given == 0:
            optional.append(APPROVAL_RULE_OPT % (subsystem, reviewers))
        else:
            provided.append(APPROVAL_RULE_OKAY % (subsystem, given, required))

    if needed:
        summary.append("Required Approvals:  \n")
        summary += needed
    if optional:
        summary.append("Optional Approvals:  \n")
        summary += optional
    if provided:
        summary.append("Satisfied Approvals:  \n")
        summary += provided

    return summary, req_approvals


def _get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest, acks, nacks,
                          req_reviewers, ar_reviewers, user_cache):
    # pylint: disable=too-many-arguments,too-many-locals,too-many-branches
    (labels, subsys_without_acks) = _get_subsys_scoped_labels(gl_project, gl_mergerequest,
                                                              acks, nacks, req_reviewers)
    summary = []
    (ack_summary, ack_count) = _show_ack_nacks(acks)
    if ack_summary:
        summary.append(f'ACKed by {ack_summary}.\n')

    if ar_reviewers:
        summary.append("\n")

    summary, req_approvals = _get_ar_acks_nacks(gl_instance, ar_reviewers, summary, user_cache)

    (nack_summary, _) = _show_ack_nacks(nacks)
    if nack_summary:
        summary.append(f'NACKed by {nack_summary}.')
        return ('NACKed', labels, ' '.join(summary))

    if subsys_without_acks or req_approvals > 0:
        return ('NeedsReview', labels, ' '.join(summary))

    approvals = gl_mergerequest.approvals.get(1)
    min_reviewers = approvals.approvals_required
    # We're still counting ack comments, so use len(ack_summary) for now, but in the future,
    # it should indeed be approvals.approvals_required - approvals.approvals_left
    # ack_count = min_reviewers - approvals.approvals_left
    if approvals.approvals_left and ack_count < min_reviewers:
        summary.append(f'Requires {min_reviewers - ack_count} more ACK(s).')
        return ('NeedsReview', labels, ' '.join(summary))

    if req_approvals > 0:
        summary.append(f'Requires {req_approvals} more Approval Rule ACK(s).')
        return ('NeedsReview', labels, ' '.join(summary))

    if gl_project.only_allow_merge_if_all_discussions_are_resolved and \
       not gl_mergerequest.changes()['blocking_discussions_resolved']:
        summary.append('All discussions must be resolved.')
        return defs.BLOCKED_SUFFIX, labels, ' '.join(summary)

    summary.append('Merge Request has all necessary Approvals.')
    return (defs.READY_SUFFIX, labels, ' '.join(summary))


def _lookup_gitlab_email(gl_instance, user_id, user_cache):
    if user_id in user_cache:
        return user_cache[user_id]

    user_cache[user_id] = gl_instance.users.get(user_id).public_email
    return user_cache[user_id]


def _lookup_submitter_and_notes(gl_instance, gl_mergerequest, user_cache):
    notes = []
    for note in gl_mergerequest.notes.list(sort='asc', order_by='created_at', as_list=False):
        email = _lookup_gitlab_email(gl_instance, note.author['id'], user_cache)
        notes.append((note.updated_at, note.body, email, note.author['username']))

    return notes


def _get_last_diff_timestamp(gl_mergerequest):
    """Walk all of the diff versions and get the latest created_at."""
    last_diff_ts = None
    diffs = gl_mergerequest.diffs.list()
    for diff in diffs:
        if not last_diff_ts or last_diff_ts < diff.created_at:
            last_diff_ts = diff.created_at

    return last_diff_ts


def _filter_stale_reviewers(gl_mergerequest, stale):
    # this should filter out reviewers added excplicity by non-bot users
    user_added = []
    bot_added = []
    notes = gl_mergerequest.notes.list(as_list=False)
    for note in notes:
        n_author = note.author['username']
        n_body = note.body
        if n_body.startswith("requested review from"):
            if n_author in defs.BOT_ACCOUNTS:
                for user in stale:
                    if f'@{user}' in n_body:
                        bot_added.append(user)
            else:
                for user in stale:
                    if f'@{user}' in n_body:
                        user_added.append(user)

    return [x for x in stale if x not in user_added and x in bot_added]


def _unassign_reviewers(gl_mergerequest, users):
    if users and misc.is_production() and not common.mr_is_closed(gl_mergerequest):
        LOGGER.info('Unassigning users %s to MR %s', users, gl_mergerequest.iid)

        # Assign the reviewers via a quick action to avoid race conditions.
        gl_mergerequest.notes.create({'body': '/unassign_reviewer ' + ' '.join(users)})


def _get_existing_reviewers(gl_mergerequest):
    return [x['username'] for x in gl_mergerequest.reviewers]


def _emails_to_gl_user_names(gl_instance, emails):
    # Search GitLab for users with a public email address set.
    users = set([])
    for email in emails:
        if "@redhat.com" not in email and "@fedoraproject.org" not in email:
            continue
        users.update([x.username for x in gl_instance.users.list(search=email)])
    return users


def _get_stale_reviewers(gl_instance, old, req_reviewers):
    reviewers = set([])
    for reviewer_set in req_reviewers:
        reviewers.update(reviewer_set[0])
    new = _emails_to_gl_user_names(gl_instance, reviewers)
    return [x for x in old if x not in new]


def _do_assign_reviewers(gl_mergerequest, users):
    if users and misc.is_production() and not common.mr_is_closed(gl_mergerequest):
        LOGGER.info('Assigning users %s to MR %s', users, gl_mergerequest.iid)

        # Assign the reviewers via a quick action to avoid race conditions.
        cmds = [f'/assign_reviewer @{x}' for x in users]
        gl_mergerequest.notes.create({'body': '\n'.join(cmds)})


def _assign_reviewers(gl_instance, gl_mergerequest, req_reviewers, submitter_email):
    # Gather the list of expected reviewers/maintainers from owners.yaml and remove MR submitter.
    reviewers = set([])
    for reviewer_set in req_reviewers:
        reviewers.update(reviewer_set[0])

    if submitter_email in reviewers:
        reviewers.remove(submitter_email)

    users = _emails_to_gl_user_names(gl_instance, reviewers)
    # A submitter w/o a public email can end up in reviewers still
    if gl_mergerequest.author['username'] in users:
        users.remove(gl_mergerequest.author['username'])

    LOGGER.info('Minimum reviewers on MR %s: %s', gl_mergerequest.iid, users)

    # Check the GitLab merge request reviewers field and remove users that are already assigned.
    for reviewer in gl_mergerequest.reviewers:
        if reviewer['username'] in users:
            users.remove(reviewer['username'])

    _do_assign_reviewers(gl_mergerequest, users)


def _report_stale_approvals(gl_instance, gl_mergerequest, invalid):
    """Report any invalidated acks to the MR to re-tag those users."""
    if not invalid:
        return

    invalidated_users = _emails_to_gl_user_names(gl_instance, invalid)
    gl_users = f"@{' @'.join(invalidated_users)}"
    body = STALE_APPROVALS_SUMMARY % gl_users
    if misc.is_production():
        gl_mergerequest.notes.create({'body': body})
    LOGGER.info("Reported stale Approvals from: %s", gl_users)


def get_reviewers_from_approval_rules(gl_mergerequest):
    """Read the MR's approval rules for custom-added required reviewers."""
    mr_approval_rules = gl_mergerequest.approval_rules.list(as_list=False)
    current_approvals = gl_mergerequest.approvals.get(1)
    ar_reviewers = []
    for rule in mr_approval_rules:
        # skip our default All Members rules
        if rule.name == "All Members":
            continue
        name = rule.name
        eligible_approvers = rule.eligible_approvers
        LOGGER.debug("Rule: %s, Eligible: %s (required: %d)",
                     name, rule.eligible_approvers, rule.approvals_required)
        LOGGER.debug("Current approvals: %s", current_approvals.approved_by)
        req = rule.approvals_required
        given = 0
        for approver in current_approvals.approved_by:
            for eligible in rule.eligible_approvers:
                if approver['user']['id'] == eligible['id']:
                    given += 1
                    break
        approvers = []
        ids = []
        for approver in eligible_approvers:
            approvers.append(approver['username'])
            ids.append(approver['id'])
        LOGGER.debug("Rule: %s, requires %s more approval(s) from set (%s )", name, req, approvers)
        ar_reviewers.append([name, req, given, approvers, ids])

    return ar_reviewers


def process_merge_request(gl_instance, gl_project, gl_mergerequest, owners_parser, rhkernel_src,
                          create_gl_status_note, approval_data):
    # pylint: disable=too-many-arguments,too-many-locals
    """Process a merge request."""
    if misc.is_production() and gl_mergerequest.draft:
        LOGGER.info("Not running ack/approval hook, MR %s is in draft state", gl_mergerequest.iid)
        return

    dep_label = cdlib.set_dependencies_label(gl_instance, gl_project, gl_mergerequest)
    if dep_label not in gl_mergerequest.labels:
        gl_mergerequest = gl_project.mergerequests.get(gl_mergerequest.iid)
    if dep_label == f"Dependencies::{defs.READY_SUFFIX}":
        changed_files = [change['new_path'] for change in gl_mergerequest.changes()['changes']]
    else:
        changed_files = cdlib.get_filtered_changed_files(gl_mergerequest)

    # Update the path_list to include corresponding Kconfig files
    changed_files.extend(common.process_config_items(rhkernel_src, changed_files))
    LOGGER.debug('changed_files: %s', changed_files)

    # If get_ark_reviewers() returns any reviewers just use that as our reviewers set.
    reviewers = get_ark_reviewers(gl_project.id, gl_mergerequest, changed_files)
    required = None

    user_cache = {}
    submitter_email = _lookup_gitlab_email(gl_instance, gl_mergerequest.author['id'], user_cache)

    old_reviewers = _get_existing_reviewers(gl_mergerequest)
    LOGGER.debug("Old reviewers: %s", old_reviewers)

    _ensure_base_approval_rule(gl_project, gl_mergerequest)

    if not reviewers:
        reviewers, required = _get_reviewers(gl_instance, gl_mergerequest, changed_files,
                                             owners_parser, submitter_email)
    LOGGER.debug("All reviewers: %s\nRequired reviewers: %s", reviewers, required)

    ar_reviewers = get_reviewers_from_approval_rules(gl_mergerequest)
    LOGGER.debug("Approval Rules Reviewers: %s", ar_reviewers)

    stale_reviewers = _get_stale_reviewers(gl_instance, old_reviewers, reviewers)
    if stale_reviewers:
        stale_reviewers = _filter_stale_reviewers(gl_mergerequest, stale_reviewers)
        LOGGER.debug("Stale reviewers: %s", stale_reviewers)
        _unassign_reviewers(gl_mergerequest, stale_reviewers)

    _assign_reviewers(gl_instance, gl_mergerequest, reviewers, submitter_email)

    notes = _lookup_submitter_and_notes(gl_instance, gl_mergerequest, user_cache)

    last_diff_ts = _get_last_diff_timestamp(gl_mergerequest)
    (acks, nacks, invalid, changes) = _process_acks_nacks(approval_data, notes, last_diff_ts,
                                                          submitter_email, gl_instance, gl_project,
                                                          gl_mergerequest, rhkernel_src)
    if changes:
        cdlib.reset_blocking_test_labels(gl_instance, gl_project, gl_mergerequest, owners_parser)

    LOGGER.debug('Changed files: %s', changed_files)
    LOGGER.debug('List of possible reviewers: %s', reviewers)
    LOGGER.debug('List of ACKs: %s', acks)
    LOGGER.debug('List of NACKs: %s', nacks)
    LOGGER.debug('List of invalidated ACKs: %s', invalid)

    summary = _get_ack_nack_summary(gl_instance, gl_project, gl_mergerequest, acks, nacks,
                                    required, ar_reviewers, user_cache)
    _save(gl_instance, gl_project, gl_mergerequest, approval_data, create_gl_status_note, *summary)

    _report_stale_approvals(gl_instance, gl_mergerequest, invalid)


def _get_current_timestamp():
    return datetime.datetime.utcnow().isoformat()


def _approve_button_clicked(gl_instance, msg, tag, button_name):
    # GitLab delivers in the payload an email field and it's the Email field from
    # the user's profile. This is a different field than the Public email field on the
    # profile and the two can be different. Prefer the public email address if it's set
    # since that's used elsewhere and we've told people to make their @redhat.com address
    # their public email address.
    email = _lookup_gitlab_email(gl_instance, msg.payload['user']['id'], {})
    if not email:
        email = msg.payload['user']['email']

    name_email = f"{msg.payload['user']['name']} <{email}>"
    comment_text = f'{tag}: {name_email}\n(via {button_name} button)'
    approval_data = (_get_current_timestamp(), comment_text, email, msg.payload['user']['username'])

    return approval_data


def process_mr_webhook(gl_instance, msg, owners_parser, rhkernel_src, **_):
    """Process a merge request only if a label was changed."""
    approval_data = None
    gl_project = gl_instance.projects.get(msg.payload['project']['id'])
    if not (gl_mergerequest := common.get_mr(gl_project, msg.payload['object_attributes']['iid'])):
        return True

    gl_action = msg.payload['object_attributes'].get('action', '')
    if gl_action in ('approved', 'approval'):
        approval_data = _approve_button_clicked(gl_instance, msg, 'Acked-by', 'approve')
    elif gl_action in ('unapproved', 'unapproval'):
        approval_data = _approve_button_clicked(gl_instance, msg, 'Rescind-acked-by', 'unapprove')

    create_gl_status_note = common.has_label_changed(msg.payload, 'Acks::', common.LabelPart.PREFIX)
    process_merge_request(gl_instance, gl_project, gl_mergerequest, owners_parser, rhkernel_src,
                          create_gl_status_note, approval_data)

    return True


def process_note_webhook(gl_instance, msg, owners_parser, rhkernel_src, **_):
    """Process a note message only if a tag was specified."""
    notetext = msg.payload['object_attributes']['note']
    create_gl_status_note = common.force_webhook_evaluation(notetext, 'ack-nack')

    gl_project = gl_instance.projects.get(msg.payload['project']['id'])
    if not (gl_mergerequest := common.get_mr(gl_project, msg.payload['merge_request']['iid'])):
        return

    (tag, _, note_email) = _parse_tag(notetext)
    if tag:
        user_cache = {}
        submitter_email = _lookup_gitlab_email(gl_instance, gl_mergerequest.author['id'],
                                               user_cache)
        public_email = _lookup_gitlab_email(gl_instance, msg.payload['user']['id'], user_cache)
        tag_error = _tag_email_error_message(tag, submitter_email, note_email, public_email,
                                             msg.payload['user']['username'])
        if tag_error:
            LOGGER.warning(tag_error)
            if misc.is_production() and not common.mr_is_closed(gl_mergerequest):
                gl_mergerequest.notes.create({'body': tag_error})
            return
    elif not gl_mergerequest.blocking_discussions_resolved \
            and f'Acks::{defs.READY_SUFFIX}' in gl_mergerequest.labels:
        LOGGER.info('Processing because MR is blocked but has Acks::%s label', defs.READY_SUFFIX)
    elif gl_mergerequest.blocking_discussions_resolved \
            and f'Acks::{defs.BLOCKED_SUFFIX}' in gl_mergerequest.labels:
        LOGGER.info('Processing because MR is not blocked but has Acks::%s label',
                    defs.BLOCKED_SUFFIX)
    elif not create_gl_status_note:
        LOGGER.debug('Skipping note: %s', notetext)
        return

    process_merge_request(gl_instance, gl_project, gl_mergerequest, owners_parser, rhkernel_src,
                          create_gl_status_note, None)


WEBHOOKS = {
    'merge_request': process_mr_webhook,
    'note': process_note_webhook,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('ACK_NACK')
    parser.add_argument('--owners-yaml', **common.get_argparse_environ_opts('OWNERS_YAML'),
                        help='Path to the owners.yaml file')
    parser.add_argument('--rhkernel-src', **common.get_argparse_environ_opts('RHKERNEL_SRC'),
                        help='Directory where rh kernel will be checked out')
    args = parser.parse_args(args)
    owners_parser = common.get_owners_parser(args.owners_yaml)
    common.generic_loop(args, WEBHOOKS, owners_parser=owners_parser, rhkernel_src=args.rhkernel_src)


if __name__ == '__main__':
    main(sys.argv[1:])
